<?php
/**
 * Display a text editor
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return void
 */
function wst_display_text_area( array $layout ) {

	if ( ! $text_area ) {
		return;
	}
	include( 'views/text-editor-view.php' );
}

//SLIDER
/**
 * Display a slider
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return void
 */
function wst_display_slider( array $layout ) {

	include( 'views/slider-view.php' );
}


/**
 * Display the dotnav in sliders
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return void
 */
function wst_display_dotnav_items( array $layout ) {
	$slides             = $layout['crb_slides'];
	$total_nb_of_slides = count( $slides );
	for ( $nb_of_slides = 0; $nb_of_slides < $total_nb_of_slides; $nb_of_slides ++ ) : ?>
		<li data-uk-slideshow-item="<?php echo (int) $nb_of_slides; ?>"><a href="#"></a></li>
	<?php endfor;
}

//SLIDESHOW PANEL
/**
 * Display slideshow panel
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return void
 */
function wst_display_slideshow_panel( array $layout ) {
	if ( ! $layout['crb_slides'] ) {
		return;
	}
	include( 'views/slideshow-panel-view.php' );
}


/**
 * Display parallax area with content
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return void
 */
//Parallax
function wst_display_parallax_area( array $layout ) {

	include( 'views/parallax-view.php' );
}

/**
 * Display panel switcher
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return void
 */
//Panel Switcher
function wst_display_panel_switcher( array $layout ) {

	include( 'views/panel-switcher-view.php' );
}

/**
 * Display each item in a subnav element
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return void
 */
function wst_display_subnav_items( array $layout ) {
	$nav_items = $layout['crb_subnav'];
	foreach ( $nav_items as $nav_item ) { ?>
		<li><a href="#"><?php echo esc_html( $nav_item['crb_subnav_item'] ); ?></a></li>
	<?php }
}
/**
 * Description
 *
 * @since 1.0.0
 *
 * @param $tax
 *
 * @return array|int|WP_Error
 */
function get_switcher_terms( $tax ) {
	$items = get_terms( $tax, array(
		'fields'  => 'names',
		'orderby' => 'none',
	) );

	return $items;
}

/**
 * Get terms list of the taxonomy that filter the switcher to make a nav
 *
 * @since 1.0.0
 *
 * @param $tax
 *
 * @return void
 */
function wst_get_switcher_nav( $tax ) {
	$items = get_switcher_terms( $tax );
	foreach ( $items as $item ) { ?>
		<li><a href="#"><?php echo $item; ?></a></li>
	<?php }
}

/**
 * Get the archive of each tax term as switcher panel
 *
 * @since 1.0.0
 *
 * @param $cpt
 * @param $tax
 *
 * @return void
 */
function wst_get_switcher_content( $cpt, $tax ) {
	$items = get_switcher_terms( $tax );
	foreach ( $items as $item ) {
		$args = array(
			'tax_query' => array(
				array(
					'taxonomy' => $tax,
					'field'    => 'slug',
					'terms'    => $item,
				),
			),
			'post_type' => array( $cpt ),
		);

		$the_query = new WP_Query( $args );

// The Loop
		if ( $the_query->have_posts() ) {
			echo '<li class="uk-grid uk-grid-width-medium-1-2 uk-margin-large-top">';
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				include 'views/switcher-content-item-view.php';
			}
			echo '</li>';
		}


// Reset original post data
		wp_reset_postdata();
	}

}




/**
 * Display a versatile lightbox gallery, with content and hover effects
 *
 * @since 1.0.0
 *
 * @param $layout
 *
 * @return void
 */
function wst_display_lightbox_gallery( array $layout ) {
	if ( ! $layout['crb_gallery_items'] ) {
		return;
	}
	include( 'views/lightbox-gallery-view.php' );
}


function wst_display_icon_text_boxes( array $layout ) {
	include( 'views/icon-text-boxes-view.php' );
}

function wst_display_boxes_items( array $layout, $animation_boxes ) {
	$boxes = $layout['text_boxes_items'];

	if ( ! $boxes ) {
		return;
	}
	foreach ( $boxes as $box ) {


	}

}