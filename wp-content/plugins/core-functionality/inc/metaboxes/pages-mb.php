<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'post_meta', 'price' )
         ->show_on_post_type( 'dishes' )
         ->add_fields( array(
	         Field::make( 'text', 'crb_price' ),
         ) );


Container::make('term_meta', 'Menus Images')
         ->show_on_taxonomy('menus')
         ->add_fields(array(
	         Field::make('image', 'crb_hero_tax_image',__('Hero Image',CHILD_TEXT_DOMAIN)),
         ));

Container::make( 'post_meta', 'Hero Image' )
         ->show_on_post_type(array ('page','dishes','post' ))
         ->add_fields( array(
	         Field::make('image', 'crb_hero_image',__('Hero Image',CHILD_TEXT_DOMAIN)),
         ) );