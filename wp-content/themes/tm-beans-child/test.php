<?php
//template Name: test
beans_add_smart_action( 'beans_header_after_markup', 'wst_display_menus' );
function wst_display_menus () {
	$items = get_terms( 'menus', array(
		'fields'  => 'ids',
		'orderby' => 'none'
	) );
	foreach ( $items as $item ) {


		$args = array(
			'tax_query' => array(
				array(
					'taxonomy' => 'menus',
					'field'    => 'term_taxonomy_id',
					'terms'    => $item,
				),
			),
			'post_type' => array( 'dishes' ),
		);

		$the_query = new WP_Query( $args );

// The Loop
		if ( $the_query->have_posts() ) {
			echo '<li class="uk-grid uk-grid-width-medium-1-2 uk-margin-large-top">';
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
$price = esc_html(carbon_get_the_post_meta('crb_price'));
?>
<div class="uk-panel uk-margin-top uk-animation-slide-bottom  ">
	<div class="uk-panel-badge uk-badge"><?php echo $price; ?></div>
	<h3 class="uk-panel-title"><?php the_title(); ?></h3>
	<div class="uk-margin tm-menu-item">
		<div class="uk-grid uk-grid-small">
			<div class="uk-width-medium-1-4">
				<?php the_post_thumbnail(); ?>
			</div>
			<div class="uk-width-medium-3-4 tm-switcher-content uk-flex uk-flex-middle">
				<p><?php the_excerpt(); ?></p>
			</div>
		</div>
	</div>
</div>
<?php }
			echo '</li>';
		}


// Reset original post data
		wp_reset_postdata();
	}
}


beans_load_document();