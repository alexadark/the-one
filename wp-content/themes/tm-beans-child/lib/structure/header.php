<?php
beans_add_smart_action( 'wp', 'wst_set_up_header_structure' );
function wst_set_up_header_structure() {

	beans_remove_action( 'beans_site_title_tag' );

	//sticky header
	beans_add_attribute( 'beans_header', 'data-uk-sticky', "{top:-300, animation:'uk-animation-slide-top'}" );

	beans_wrap_markup( 'beans_header', 'beans_header_wrapper', 'div', array(
		'class' => 'tm-header-wrapper uk-cover-background',
	) );


	// Breadcrumb
	beans_remove_action( 'beans_breadcrumb' );

	//Front page
	if ( is_front_page() ) {
		beans_add_attribute( 'beans_header_wrapper', 'class', 'uk-height-viewport' );
		beans_add_smart_action( 'beans_header_wrapper_append_markup', 'beans_hero_home_title' );
		function beans_hero_home_title() {
			include 'views/hero-home-title-view.php';
		}
	} else {
		beans_add_smart_action( 'beans_header_wrapper_append_markup', 'beans_hero_title' );
		function beans_hero_title() {
			include 'views/hero-title-view.php';
		}
	}


	beans_add_smart_action( 'wp_head', 'header_style' );
	function header_style() {
		$image_id = is_tax( 'menus' ) ? esc_attr( carbon_get_the_term_meta( 'menus', 'crb_hero_tax_image' ) ) :
			esc_attr( carbon_get_the_post_meta( 'crb_hero_image' ) );
		$image_url = wp_get_attachment_image_url( $image_id, 'full' );
		if(!$image_id && !is_front_page()){
			beans_add_attribute('beans_header','class','no-image');
			beans_add_attribute('beans_header_wrapper','class','no-image');
		}
		if ( is_front_page() || ! $image_id ) {
			return;
		} ?>
		<style>
			.tm-header-wrapper {
				background:linear-gradient( rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.2) ), url(<?php
				 echo $image_url; ?>) no-repeat;
				.gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.2));
				height: 300px;
				width: 100%;
				-webkit-background-size: cover;
				background-size: cover;

			@media (max-width: @breakpoint-medium) {
				height:

			200px

			;
			}

			}
		</style>
	<?php }
}

