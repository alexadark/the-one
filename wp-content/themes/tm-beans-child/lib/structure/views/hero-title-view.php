<div class="tm-hero-title uk-text-center">
	<h1 class="uk-heading-large"
	    itemprop="headline"
	    data-uk-animatedtext><?php if ( is_tax( 'menus' ) ) {
			$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			echo $term->name;
		} else {
			the_title();
		} ?></h1>
	<?php
	$image_id  = is_tax( 'menus' ) ? esc_attr( carbon_get_the_term_meta( 'menus', 'crb_hero_tax_image' ) ) : esc_attr( carbon_get_the_post_meta( 'crb_hero_image' )) ;

	if(!$image_id ){
		include 'divider-view.php';
	}

?>

</div>