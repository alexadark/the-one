<div class="tm-hero-title uk-text-center " >
	<h1 class="uk-heading-large" data-uk-animatedtext><?php echo get_theme_mod
		('hero_textbox'); ?></h1>
	<?php include 'divider-view.php'?>
	<h3 class="tm-hero-subtitle uk-text-text-large"
	    data-uk-scrollspy="{cls:'uk-animation-slide-bottom uk-animation-1',delay:600,repeat:true}">
		<?php echo get_theme_mod('hero_subtitle'); ?>
	</h3>
</div>