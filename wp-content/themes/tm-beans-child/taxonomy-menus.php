<?php

//beans_add_smart_action( 'beans_header_wrapper_append_markup', 'wst_display_menu_name' );
function wst_display_menu_name() {
	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	echo $term->name;
}

beans_add_attribute('beans_content','class','uk-grid uk-grid-width-large-1-3 uk-grid-width-small-1-2');
beans_add_attribute('beans_content','data-uk-grid','');
beans_remove_action('beans_post_content');
beans_wrap_inner_markup('beans_post_image_item_wrap','dish_overlay', 'div', array(
	'class'=>'uk-overlay uk-overlay-hover'
));
beans_add_smart_action( 'dish_overlay_append_markup', 'dish_excerpt' );
function dish_excerpt() { ?>
	<figcaption class="uk-overlay-panel uk-overlay-bottom uk-overlay-slide-bottom">
		<?php the_excerpt();?>

	</figcaption>
<?php }

beans_add_smart_action( 'dish_overlay_prepend_markup', 'display_dish_price' );
function display_dish_price () { ?>
<div class="uk-badge"><?php echo esc_attr(carbon_get_the_post_meta('crb_price'));?></div>
<?php }


beans_load_document();